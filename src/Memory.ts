import { CreepStatus } from "./Creep";

declare global {
  interface CreepMemory {
    status: CreepStatus
  }

  interface FlagMemory { [name: string]: any }
  interface SpawnMemory { [name: string]: any }

  interface RoomMemory {
    sources?: RoomPosition[]
  }
}