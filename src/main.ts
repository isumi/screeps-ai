import { CreepStatus } from "./Creep";

export function loop() {
  const room: Room = getSpawn().room;
  if (!room.controller) {
    return
  }

  const sourceList = room.find(FIND_SOURCES);
  const source = sourceList[0];

  {
    const numScreeps = Object.keys(Game.creeps).length;
    if (numScreeps < 2) {
      createCreep();
      return;
    }
  }

  for (const name in Memory.creeps) {
    if (!Game.creeps[name]) {
      delete Memory.creeps[name];
    }
  }

  for (const creepName of Object.keys(Game.creeps)) {
    const creep = Game.creeps[creepName];
    if (creep.spawning) {
      continue;
    }
    let status = creep.memory.status;
    if (!status) {
      status = CreepStatus.UPGRADE;
    }
    if (creep.store.energy === 0) {
      status = CreepStatus.HARVEST;
    } else if (creep.store.energy === creep.store.getCapacity()) {
      status = CreepStatus.UPGRADE;
    }

    if (status === CreepStatus.UPGRADE) {
      if (creep.upgradeController(room.controller) === ERR_NOT_IN_RANGE) {
        creep.moveTo(room.controller.pos);
      }
    } else if (status === CreepStatus.HARVEST) {
      const err = creep.harvest(source);
      if (err !== OK) {
        if (err === ERR_NOT_IN_RANGE) {
          creep.moveTo(source.pos);
        } else {
          console.log("unexpected error: " + err.toString());
        }
      }
    }

    creep.memory.status = status;
  }
}

function getSpawn(): StructureSpawn {
  return Game.spawns["Spawn1"];
}

export function createCreep() {
  const spawn: StructureSpawn = getSpawn();
  const creep = spawn.createCreep([WORK, CARRY, MOVE, MOVE]);
  if (typeof creep !== "string") {
    console.log("fail to create creep, err code: " + creep);
  } else {
    console.log("creating creep '" + creep + "'");
  }
}

/*
creep区分角色

预设几种creep类型

*/