module.exports = function (grunt) {
  var config = require('./local/.screeps.json')
  grunt.initConfig({
    ts: {
      default: {
        tsconfig: './tsconfig.json'
      }
    },
    screeps: {
      options: {
        email: config.email,
        password: config.password,
        branch: config.branch,
        ptr: config.ptr
      },
      dist: {
        src: ['dist/*.js']
      }
    }
  });
  grunt.loadNpmTasks('grunt-screeps');
}