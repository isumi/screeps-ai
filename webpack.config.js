const path = require('path');
const ClosurePlugin = require('closure-webpack-plugin');

module.exports = {
  target: 'node',
  entry: './src/main.ts',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    library: 'ScreepsAI',
    libraryTarget: 'umd'
  },
  optimization: {
    minimizer: [
      new ClosurePlugin({ mode: 'STANDARD' }, {
      })
    ]
  }
};